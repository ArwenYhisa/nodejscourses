let number1 = "2";
let number2 = "5";

console.log("" + (parseInt(number1) + parseInt(number2)));

function add(number) {
    if (number === "") {
        return "0";
    }
    let nb = number.split(',');
    let sum = 0;
    nb.forEach(element => {
        let n = parseFloat(element);
        if (isNaN(n)){
            console.log("Wrong input: " + element)
            return
        }
        sum += n;
    });
   return parseFloat(sum.toFixed(1)).toString();
}

console.log(add("1.1, 2.2"))